// Header
#include "im.h"
#include <math.h>	// absや数学関数を使うヘッダ

void clamp(int& RDst, int& GDst, int& BDst)
{
	if (RDst > 255)
	{        // 上限チェック
		RDst = 255;
	}

	else if (RDst < 0)
	{	// 下限チェック
		RDst = 0;
	}

	if (GDst > 255) 
	{        // 上限チェック
		GDst = 255;
	}

	else if (GDst < 0)
	{	// 下限チェック
		GDst = 0;
	}

	if (BDst > 255) 
	{        // 上限チェック
		BDst = 255;
	}

	else if (BDst < 0)
	{	// 下限チェック
		BDst = 0;
	}
}
int quantize(int Ysig, int n)
{
	int sakai[9] = { 0 };
	int outValues[10] = { 0 };
	int delta = 255 / n;
	outValues[0] = 255;
	int i;
	for (i = 0; i < n; i++)
	{
		outValues[i + 1] = outValues[i] - delta;
		sakai[i] = outValues[i] - delta / 2;
	}
	outValues[n] = 0;
	int val2th;
	for (i = 0; i < n; i++)
	{
		if (Ysig > sakai[i])
		{
			val2th = outValues[i];
			break;
		}
	}
	if (i == n)
	{
		val2th = outValues[n];
	}
	return val2th;
}
void imFilterAverage(ms2dImage *imgSrc, ms2dImage *imgDst)//平均化
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[25] = {-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1,-1, 0, 1};	// 横方向移動量
	int dy[25] = {-1,-1,-1,-1,-1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1};	// 縦方向移動量
	int k[25]  = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};	// フィルタ係数

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
			}

			// 合計値を9で割って、平均値を求める
			int RDst = RSum / 9;
			int GDst = GSum / 9;
			int BDst = BSum / 9;

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterEmboss(ms2dImage *imgSrc, ms2dImage *imgDst)//エンボス
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int k[9]  = { 0, 0, 0, 0, 1, 0, 0, 0, -1 };	// フィルタ係数

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
			}

			// 合計値を9で割って、平均値を求める
			int RDst = RSum + 127;
			int GDst = GSum + 127;
			int BDst = BSum + 127;

			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterSherpen(ms2dImage *imgSrc, ms2dImage *imgDst)//先鋭化
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int k[9]  = { 0,-1, 0,-1, 5,-1, 0,-1, 0 };	// フィルタ係数

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 6; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
			}

			// 合計値を9で割って、平均値を求める
			int RDst = RSum / 2;
			int GDst = GSum / 2;
			int BDst = BSum / 2;

			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterLaplacian(ms2dImage *imgSrc, ms2dImage *imgDst)//ラプラシアン
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int k[9]  = { 0, 1, 0, 1,-4, 1, 0, 1, 0 };	// フィルタ係数

	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i];
			}

			int RDst = RSum + 127;
			int GDst = GSum + 127;
			int BDst = BSum + 127;

			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterEdge(ms2dImage *imgSrc, ms2dImage *imgDst)//エッジ検出
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int k[9]  = { 0, 0, 0, 0,-1, 1, 0, 0, 0 };	// フィルタ係数
	int m[9]  = { 0, 0, 0, 0,-1, 0, 0, 1, 0 };
	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;
			int RSom = 0;
			int GSom = 0;
			int BSom = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i] ;
				RSom += pxSrc[y+dy[i]][x+dx[i]].R * m[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				GSom += pxSrc[y+dy[i]][x+dx[i]].G * m[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i] ;
				BSom += pxSrc[y+dy[i]][x+dx[i]].B * m[i];
			}
			
			int RDst = abs(RSum)+abs(RSom);
			int GDst = abs(GSum)+abs(GSom);
			int BDst = abs(BSum)+abs(BSom);
			
			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterSorble(ms2dImage *imgSrc, ms2dImage *imgDst)//ソーベル
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int k[9]  = {-1, 0, 1,-2, 0, 2,-1, 0, 1 };	// フィルタ係数
	int m[9]  = {-1,-2,-1, 0, 0, 0, 1, 2, 1 };
	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int RSum = 0;
			int GSum = 0;
			int BSum = 0;
			int RSom = 0;
			int GSom = 0;
			int BSom = 0;

			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
				RSum += pxSrc[y+dy[i]][x+dx[i]].R * k[i] ;
				RSom += pxSrc[y+dy[i]][x+dx[i]].R * m[i];
				GSum += pxSrc[y+dy[i]][x+dx[i]].G * k[i];
				GSom += pxSrc[y+dy[i]][x+dx[i]].G * m[i];
				BSum += pxSrc[y+dy[i]][x+dx[i]].B * k[i] ;
				BSom += pxSrc[y+dy[i]][x+dx[i]].B * m[i];
			}
			
			int RDst = abs(RSum)+abs(RSom);
			int GDst = abs(GSum)+abs(GSom);
			int BDst = abs(BSum)+abs(BSom);

			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterMedian(ms2dImage *imgSrc, ms2dImage *imgDst)//メディアン
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	int dx[9] = {-1, 0, 1,-1, 0, 1,-1, 0, 1 };	// 横方向移動量
	int dy[9] = {-1,-1,-1, 0, 0, 0, 1, 1, 1 };	// 縦方向移動量
	int Ysig[9] = {0,0,0,0,0,0,0,0,0};
	// 1ピクセルずつ、すべてのピクセルを処理
	// 空間フィルタは周囲のピクセルを参照するため、画面の端は処理しない
	for(int y = 1; y < hgt-1; y++)
	{
		for(int x = 1; x < wid-1; x++)
		{
			// 合計値を入れる変数（最初は0に初期化)
			int id = 0;
			int max1=0;
				
			// 周辺9画素を計算に使う
			for(int i = 0; i < 9; i++)
			{
				// 元画像pxSrcの(x+dx[i], y+dy[i])座標の色値を得て、フィルタ係数をかける
			 	Ysig[i]=(0.299* pxSrc[y+dy[i]][x+dx[i]].R + 0.587*pxSrc[y+dy[i]][x+dx[i]].G + 0.114*pxSrc[y+dy[i]][x+dx[i]].B);
			}

			for(int v =0; v<5; v++)
			{
				max1=0;
				for(int i = 0; i < 9; i++)
				{
					if (max1<Ysig[i])
					{
						max1=Ysig[i];
						id = i;
					}
				}
				Ysig[id]=-1;
			}
			int RDst =max1 ;
			int GDst =max1 ;
			int BDst =max1 ;
			
			clamp(RDst, GDst, BDst);

			// 結果画像pxDstの(x,y)の色値を更新
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterContrast(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			double lowTh =64.0;
			double highTh = 192.0;
			int Rval = pxSrc[y][x].R;
			if(Rval <= lowTh){
				Rval = 0;
			}
			else if(Rval >= highTh)
			{
				Rval = 255;
			}
			else 
			{
				Rval = (int)(255/(highTh - lowTh)*(Rval - lowTh));
			}
			
			int Gval = pxSrc[y][x].G;
			if(Gval <= lowTh)
			{
				Gval = 0;
			}
			else if(Gval >= highTh)
			{
				Gval = 255;
			}
			else 
			{
				Gval = (int)(255/(highTh - lowTh)*(Gval - lowTh));
			}
			
			int Bval = pxSrc[y][x].B;
			if(Bval <= lowTh)
			{
				Bval = 0;
			}
			else if(Bval >= highTh)
			{
				Bval = 255;
			}
			else 
			{
				Bval = (int)(255/(highTh - lowTh)*(Bval - lowTh));
			}

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = Rval;
			pxDst[y][x].G = Gval;
			pxDst[y][x].B = Bval;
		}
	}
}
void imFilterGray(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ

	// 1ピクセルずつ、すべてのピクセルを処理
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// R,G,Bの平均値を求める
			int ave  = (Rval + Gval + Bval) / 3;

			// 結果画像pxDstの(x,y)の色値を全てaveにする = 灰色
			pxDst[y][x].R = ave;
			pxDst[y][x].G = ave;
			pxDst[y][x].B = ave;
		}
	}
}
void imFilterRed(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = Rval;
			pxDst[y][x].G = 0;
			pxDst[y][x].B = 0;
		}
	}
}
void imFilterGreen(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = 0;
			pxDst[y][x].G = Gval;
			pxDst[y][x].B = 0;
		}
	}
}
void imFilterBlue(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = 0;
			pxDst[y][x].G = 0;
			pxDst[y][x].B = Bval;
		}
	}
}
void imFilterNega(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int RDst = 255 - Rsrc;
			int GDst = 255 - Gsrc;
			int BDst = 255 - Bsrc;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = RDst;
			pxDst[y][x].G = GDst;
			pxDst[y][x].B = BDst;
		}
	}
}
void imFilterMono(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = Ysig;
			pxDst[y][x].G = Ysig;
			pxDst[y][x].B = Ysig;
		}
	}
}
void imFilterCyan(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = 0;
			pxDst[y][x].G = Gval;
			pxDst[y][x].B = Bval;
		}
	}
}
void imFilterMagenta(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = Rval;
			pxDst[y][x].G = 0;
			pxDst[y][x].B = Bval;
		}
	}
}
void imFilterYellow(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for (int y = 0; y < hgt; y++)
	{
		for (int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rval = pxSrc[y][x].R;
			int Gval = pxSrc[y][x].G;
			int Bval = pxSrc[y][x].B;

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = Rval;
			pxDst[y][x].G = Gval;
			pxDst[y][x].B = 0;
		}
	}
}
void imFilter2(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 1);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter3(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ

	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 2);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter4(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	int sakai[3] = { 192, 128, 64};
	int outValues[4] = { 255, 170, 128, 0 };
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 3);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter5(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	int sakai[4] = {204, 153, 102, 51};
	int outValues[5] = {255, 192, 128, 64, 0};
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 4);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter6(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	int sakai[5] = {215, 172, 129, 86, 43};
	int outValues[6] = {255, 204, 153, 102, 51, 0};
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 5);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter7(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	int sakai[6] = {216, 180, 144, 108, 72, 36};
	int outValues[7] = {255, 215, 172, 129, 86, 43, 0};
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 6);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter8(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 7);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter9(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 8);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}
void imFilter0(ms2dImage *imgSrc, ms2dImage *imgDst)
{
	// Dstにコピー
	ms2dImageCopy(imgSrc, imgDst);

	// 画像サイズを取得
	int wid = imgDst->width;	// 幅
	int hgt = imgDst->height;	// 高さ
	// ピクセルアクセスの準備
	ms2dColorMatrix pxSrc = imgSrc->pixels;	// 元画像のピクセルデータ
	ms2dColorMatrix pxDst = imgDst->pixels;	// 結果画像のピクセルデータ
	for(int y = 0; y < hgt; y++)
	{
		for(int x = 0; x < wid; x++)
		{
			// 元画像pxSrcの(x, y)座標の色値を得る
			int Rsrc = pxSrc[y][x].R;
			int Gsrc = pxSrc[y][x].G;
			int Bsrc = pxSrc[y][x].B;
			int Ysig = (int)(0.299*Rsrc + 0.587*Gsrc + 0.114*Gsrc);
			int val2th = quantize(Ysig, 9);

			// 結果画像pxDstの(x,y)の色値Rはそのまま,G,Bを0にする
			pxDst[y][x].R = val2th;
			pxDst[y][x].G = val2th;
			pxDst[y][x].B = val2th;
		}
	}
}