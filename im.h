// Header
#include <stdio.h>			// 標準出力
#include <stdlib.h>			// 標準ライブラリ
#include <math.h>			// 数学ヘッダ
#include <GL/glut.h>		// glut
#include <msgl.h>			// msgl

// 平均化フィルタ
void imFilterAverage(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterEmboss(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterSherpen(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterLaplacian(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterEdge(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterSorble(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterMedian(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterContrast(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterGray(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterRed(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterGreen(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterBlue(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterNega(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterMono(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterMagenta(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterCyan(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilterYellow(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter2(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter3(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter4(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter5(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter6(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter7(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter8(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter9(ms2dImage *imgSrc, ms2dImage *imgDst);
void imFilter0(ms2dImage *imgSrc, ms2dImage *imgDst);