// Header
#include "im.h"
#include <math.h> 
// プロトタイプ宣言
void printHelp( char *);
GLvoid initgfx( GLvoid );
GLvoid drawScene( GLvoid );
GLvoid reshape( GLsizei, GLsizei );
GLvoid keyboard( GLubyte, GLint, GLint );

// キーボードASCII値
#define KEY_ESC	27

// ウィンドウのサイズ
static int winWidth;	// ウィンドウの横幅
static int winHeight;	// ウィンドウの高さ

// テクスチャデータ
static ms2dImage *imgSrc = NULL;		// 元画像のデータ
static ms2dImage *imgDst = NULL;		// 結果画像のデータ

void main( int argc, char *argv[] )
{
	// GLUTにmainの引数渡して初期化
    glutInit( &argc, argv );

	// GLUTスクリーンサイズの取得
    GLsizei width  = glutGet( GLUT_SCREEN_WIDTH );
    GLsizei height = glutGet( GLUT_SCREEN_HEIGHT );
	// ウィンドウの諸設定
    glutInitWindowPosition( width / 8, height / 8 );
    glutInitWindowSize( width * 6 / 8, height * 6 / 8 );
	// 色設定
	glutInitDisplayMode( GLUT_RGBA | GL_DOUBLE );
	// ウィンドウ作成(argv[0]はタイトル)
    glutCreateWindow( argv[0] );

	// 元画像読み込み
	imgSrc = ms2dImageCreateWithFile("hall.bmp");
	// 結果画像を用意
	imgDst = ms2dImageCreate(1, 1);

	// ヘルプメッセージ
	printHelp( argv[1] );
	// 初期化処理
	initgfx();

	// 再描画関数の定義
	glutKeyboardFunc( keyboard );
	glutReshapeFunc( reshape );
	glutDisplayFunc( drawScene );
	
	// メインループ（GLUTに委譲）
    glutMainLoop();
}

// ヘルプメッセージ
void printHelp( char *progname )
{
	fprintf(stdout,
		"\n%s - 画像処理02:空間フィルタ\n\n"
		"[L] - 画像読み込み\n"
		"[S] - 画像保存\n"
		"[a] - 平均化\n"
		"[e] - エンボス\n"
		"[s] - 鮮鋭化\n"
		"[l] - ラプラシアン\n"
		"[d] - エッジ抽出\n"
		"[o] - ソーベルフィルタ\n"
		"[M] - メディアン\n"		
		"[t] - 階調変換\n"
		"[h] - 灰色化\n"
		"[r] - 赤抽出\n"
		"[g] - 緑抽出\n"
		"[b] - 青抽出\n"
		"[n] - ネガ化\n"		
		"[1] - モノクロ化\n"
		"[c] - シアン抽出\n"
		"[m] - マゼンタ抽出\n"
		"[y] - イエロー抽出\n" 
		"[2] - ２値化\n"
		"[3] - ３値化\n"
		"[4] - ４値化\n"
		"[5] - ５値化\n"
		"[6] - ６値化\n"
		"[7] - ７値化\n"
		"[8] - ８値化\n"
		"[9] - ９値化\n"
		"[0] - １０値化\n"
		"ESC - プログラムの終了\n\n",
		progname);
}

// 初期化処理
GLvoid initgfx( GLvoid )
{
	// 背景の色
	glClearColor(0.85, 0.88, 0.9, 1.0);
}

// 画面サイズ変更
GLvoid reshape(GLsizei width, GLsizei height)
{
	// ウィンドウ枠のビューポートを設定
    glViewport( 0, 0, width, height );
	// ウィンドウのサイズの更新
	winWidth  = width;
	winHeight = height;
}

// キーボード処理
GLvoid keyboard(GLubyte key, GLint x, GLint y)
{
	switch(key)
	{
		case 'L':
		{
			// 読込用ファイルダイアログを開く
			char path[256];
			if(msglOpenDialog(path))
			{
				// imgSrcにファイルを読み込む
				ms2dImageLoadFile(imgSrc, path);
				// msglのテクスチャを新しいデータに差し替え
				msglReplaceTexture(imgSrc);
				// 再描画
				glutPostRedisplay();
			}
			break;
		}
		case 'S':
		{
			// 保存用ファイルダイアログを開く
			char path[256];
			if(msglSaveDialog(path))
			{
				// imgDstをファイルに保存する
				ms2dImageSaveFile(imgDst, path);
			}
			break;
		}
		case 'a':
		{
			// 平均化フィルタ
					imFilterAverage(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'e':
		{
			// エンボスフィルタ
			imFilterEmboss(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();

			break;
		}
		case 's':
		{
			// 鮮鋭化フィルタ
			imFilterSherpen(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'l':
		{
			// ラプラシアンフィルタ
			imFilterLaplacian(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'd':
		{
			// エッジ抽出フィルタ
			imFilterEdge(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'o':
		{
			// Sobelフィルタ
			imFilterSorble(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}		
		case 'M':
		{
			// メディアンフィルタ
			imFilterMedian(imgSrc, imgDst);
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 't':
		{
			// 階調変換
			imFilterContrast(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'h':
		{
			// 灰色抽出
			imFilterGray(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'r':
		{
			// Red抽出
			imFilterRed(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'g':
		{
			// Green抽出
			imFilterGreen(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'b':
		{
			// Blue抽出
			imFilterBlue(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'c':
		{
			// cyan抽出
			imFilterCyan(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'm':
		{
			// マゼンタ抽出
			imFilterMagenta(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case 'y':
		{
					// イエロー抽出
					imFilterYellow(imgSrc, imgDst);
					// msglのテクスチャを新しいデータに差し替え
					msglReplaceTexture(imgDst);
					// 再描画
					glutPostRedisplay();
					break;
		}
		case 'n':
		{
			// ネガ
			imFilterNega(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '1':
		{
			// Y信号
			imFilterMono(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '2':
		{
			// 2値化
			imFilter2(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '3':
		{
			// 2値化
			imFilter3(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '4':
		{
			// 2値化
			imFilter4(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '5':
		{
			// 2値化
			imFilter5(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '6':
		{
			// 2値化
			imFilter6(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '7':
		{
			// 2値化
			imFilter7(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '8':
		{
			// 2値化
			imFilter8(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '9':
		{
			// 2値化
			imFilter9(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}
		case '0':
		{
			// 2値化
			imFilter0(imgSrc, imgDst);
			// msglのテクスチャを新しいデータに差し替え
			msglReplaceTexture(imgDst);
			// 再描画
			glutPostRedisplay();
			break;
		}

		// プログラム終了
		case KEY_ESC:
		{
			exit(0);
		}
	}
}

// 描画処理
GLvoid drawScene( GLvoid )
{
	glClear( GL_COLOR_BUFFER_BIT );

	// msglの行列をセットする
	msglPushDefaultMatrix(winWidth, winHeight);
	{
		// 元画像のmsglテクスチャ平面を表示
		msglTexQuad(imgSrc);

		// 結果画像のmsglテクスチャ平面を表示
		glTranslatef(imgSrc->width+10, 0, 0);
		msglTexQuad(imgDst);
	}
	// msglの行列をクリアする
	msglPopMatrix();

	// 強制更新
	glutSwapBuffers();
}